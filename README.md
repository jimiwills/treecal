Currently just notes here.  Whole process derived from 
http://www.faeriefaith.net/treecal.html

Process:

2nd Feb is Imbolc.  (why is this dependent on the Greg calendar?)
Go back to the previous new moon ... this is the start of Birch
Go back to the previous new moon ... 
If the next winter solstice <30 days away?
    Ash starts on the next winter solstice
Else If the prev winter solstice <30 days away?
    Ash starts now (the new moon)
    Days apart are between previous solstice and day before this new moon.
Else 
    something went very wrong!



To get solstice:

use strict; use warnings;
use Astro::Utils;
 
print "Mar'2015 Equinox  (UTC): ", calculate_equinox ('mar', 'utc', 2015),"\n";
print "Mar'2015 Equinox  (TDT): ", calculate_equinox ('mar', 'tdt', 2015),"\n";
 
print "Jun'2015 Solstice (UTC): ", calculate_solstice('jun', 'utc', 2015),"\n";
print "Jun'2015 Solstice (TDT): ", calculate_solstice('jun', 'tdt', 2015),"\n";
 
print "Sep'2015 Equinox  (UTC): ", calculate_equinox ('sep', 'utc', 2015),"\n";
print "Sep'2015 Equinox  (TDT): ", calculate_equinox ('sep', 'tdt', 2015),"\n";
 
print "Dec'2015 Solstice (UTC): ", calculate_solstice('dec', 'utc', 2015),"\n";
print "Dec'2015 Solstice (TDT): ", calculate_solstice('dec', 'tdt', 2015),"\n";



TO get moons:

use Astro::MoonPhase;

        ( $MoonPhase,
          $MoonIllum,
          $MoonAge,
          $MoonDist,
          $MoonAng,
          $SunDist,
          $SunAng ) = phase($seconds_since_1970);

        @phases  = phasehunt($seconds_since_1970);

        ($phase, @times) = phaselist($start, $stop);
        
        
        
observations...

the Astro::MoonPhase module takes into account the angle
of the sun, etc.

The angle of the sun at winter solstice is:

0.541978790110419
0.541978628878365
0.541979160981201

so we can use this feature to find the solstices
bounding a year.

or we can use the easier version and just calculate it using a
linear equation!

