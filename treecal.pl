#!/usr/bin/perl

# this program works out new moon dates with an error of up to 14 hours
# depending on periodic perturbations. Seems to be about 5 hours ahead of
# dates at timeanddate.com.   (http://www.hermetic.ch/cal_sw/ve/ve.htm)

#print "Content-type: text/html\n\n";


use HTTP::Date;

my $y2k = str2time("Jan  1 00:00:00 2000") + 0.000739 * 24 * 60 * 60;
my $yearlength = 365 + 97/400 ;
my $firstepochequinox = str2time("1970-12-22 06:39") / (60 * 60 * 24);


# ########
# # USER TESTING AREA:

# # what year to print:
# $calYear = $ENV{'QUERY_STRING'} || (gmtime(time()))[5] + 1900;
# # start sending html
# print "<html>".
#     "<head><style>body {color:#006000; font-family:century;}\n".
#     ".CY {font-size: 150pt}\n".   # Calendar Year
#     ".MN {font-size: 30pt; font-weight: bold;}\n".   # Month Name
#     ".WD {font-size: 8pt}\n".   # Week Day
#     ".MD {font-size: 44pt}\n".   # Month Day
#     ".DM {font-size: 8pt}\n".   # Day Message
#     ".GD {font-size: 8pt}\n".   # Greg. Date
#     ".MB {height: 490px;}\n".   # Month Box
#     ".MIB {height: 460px; border: solid green 1px;}\n".   # Month Inner Box
#     "a {font-size: 8pt; color:#c09030;}\n".   # Greg. Date
#     "</style></head>\n".
#     "<body><div class=MB><div class=MIB>\n";
# # print some links to other years:
# my $backten = $calYear - 10;
# my $backone = $calYear - 1;
# my $foreone = $calYear + 1;
# my $foreten = $calYear + 10;
# print "<a href=treecal.pl?$backten>&lt;&lt;$backten</a>\n";
# print "<a href=treecal.pl?$backone>&lt;$backone</a> | \n";
# print "<a href=treecal.pl?$foreone>$foreone&gt;</a>\n";
# print "<a href=treecal.pl?$foreten>$foreten&gt;&gt;</a>\n";

# # print a little message:
# print <<MESSAGE;

# <h1>The Tree Calendar</h1>
# <p>You have chosen <u>$calYear</u>. If this is not the year you want then please use
# the links above to get to the correct year.
# <p>The tree calendar has various incarnations
# but mine is based on the Celtic Tree Calendar by Linda Kerr which may be found at
# <a href=http://www.auburn.edu/~kerrlin/treecal.html>http://www.auburn.edu/~kerrlin/treecal.html</a> in
# which the months follow the lunations and
# February 2, or Imbolc, always falls on the Rowan moon, the 2nd lunation.
# <p>I also had help from <a href=http://www.timeanddate.com/>http://www.timeanddate.com/</a> and
#  <a href=http://www.hermetic.ch/cal_sw/ve/ve.htm>http://www.hermetic.ch/cal_sw/ve/ve.htm</a>
#  as references for lunation times.
#  <p>Various pages at <a href=http://en.wikipedia.org/>http://en.wikipedia.org/</a> helped
#  me write the algorithms for figuring out when the lunations and solstices are.
# <p>The programming of this calendar was done in perl (<a href=http://www.perl.org/>href=http://www.perl.org/</a>)
# and the text editor used was syn (<a href=http://syn.sourceforge.net/>http://syn.sourceforge.net/</a>).
# <p>In Internet Explorer 6 a print preview shows that each printed page has exactly 2 boxes
# on it, so you should be able to print the calendar for any year and cut out the months without
# needing to cellotape together any months that were split across pages.
# <p>Jimi-Carlo Bukowski-Wills

# MESSAGE

# print "</div></div>\n";

# # print the year:
# print "<div class=MB><div class=MIB><div class=CY>$calYear</div>Tree Calendar</div></div>\n";

# # call the function
# my $y = MakeYear($calYear);
# # define some names
# @corr = ("Days Apart",qw(Birch Rowan Ash Alder Willow Hawthorn Oak Holly Hazel Vine
#             Ivy Reed Elder));
# # these are the short versions used in the machinery below
# @months = qw(DAp Bir Row Ash Ald Wil Haw Oak Hol Haz Vin Ivy Bla Eld);

# foreach $m(@months){
#     # first figure out what the real name of the month is...
#     foreach $i(0..$#months){
#         if($m eq $months[$i]){
#             $monthname = $corr[$i];
#         }
#     }
#     # print the top of the table, with month name
#     print "<div class=MB><div class=MIB><table cellspacing=1 cellpadding=1 border=0>" .
#                    "<tr><td colspan=8 bgcolor=#c0ffc0><span class=MN>$monthname</span></td></tr><tr>\n";
#     # next print some day names:
#     print "<td align=center><span class=WD>$_</span></td>" foreach qw(Moonday Tirsday Odinsday Thorsday Friggasday Lokiday Sunday Mooneve);
#     print "</tr><tr>";
#     # now print the days out with the Greg date too, or any special message.
#     foreach $d(sort keys %{$y->{$m}}){
#         print "<tr>\n" if ($d eq '08' || $d eq '16' || $d eq '23');
#         my $cd = $y->{$m}->{$d}->[3];
#         my $cm = $y->{$m}->{$d}->[4] + 1;
#         my $cy = $y->{$m}->{$d}->[5] + 1900;
#         my $message = $y->{$m}->{$d}->[9];
#         print "<!-- $monthname/$d = $cd/$cm/$cy -->\n";
#         print "<td align=center bgcolor=#c0ffc0><span class=MD>$d</span><br>";
#         print "<font color=#c09030><span class=DM>$message</span></font></td>" if $message;
#         print "<span class=GD>$cd/$cm/$cy</span></td>\n" unless $message;
#         print "</tr>\n" if ($d eq '07' || $d eq '15' || $d eq '22');
#     }
#     print "</tr></table></div></div>\n";
# }





## Extra functions

sub MakeYear {
    my ($y) = @_;
    my $gmtime = $y ? "Jan 1 00:00:00 $y" : gmtime(time());
    my $year;
    my $gmfirst = gmtime(str2time($gmtime) - TimeSinceEquinox($gmtime));
    
    # work out dayes for solar quarters
    my $winsol = join("/",(gmtime(str2time($gmtime) - TimeSinceEquinox($gmtime)))[3..4]);
    my $sumsol = join("/",(gmtime(str2time($gmtime) - TimeSinceEquinox($gmtime) + $yearlength*24*60*60*0.5))[3..4]);
    my $verneq = join("/",(gmtime(str2time($gmtime) - TimeSinceEquinox($gmtime) + $yearlength*24*60*60*0.25))[3..4]);
    my $auteq = join("/",(gmtime(str2time($gmtime) - TimeSinceEquinox($gmtime) + $yearlength*24*60*60*0.75))[3..4]);
    
    # setup offset months for easy entry into ImportantDates
    my $jan=0,$feb=1,$mar=2,$apr=3,$may=4,$jun=5,$jul=6,$aug=7,$sep=8,$oct=9,$nov=10,$dec=11;
    
    # set up dates and messages...
    my %ImportantDates = (
        # lets sort out the solstices...
        $winsol => "Midwinter",
        $sumsol => "Midsummer",
        $verneq => "Equinox",
        $auteq  => "Equinox",
        # and now the fire festivals
        "2/$feb" => "In Milk",
        "31/$oct" => "Samhain",
        "1/$aug" => "Lammas",
        "30/$apr" => "Beltaine",
        # some other dates
        "25/$dec" => "Xmas",
        "1/$may" => "Mayday",
    );
    
    my $gmnext = $gmfirst;
    my ($fd,$fm,$fy) = TreeDate($gmfirst);
    my ($nd,$nm,$ny) = ($fd,$fm,$fy);
    while ($ny eq $fy){
        my @store = gmtime(str2time($gmnext));
        my $short = join("/",@store[3..4]);
        push @store,$ImportantDates{$short} if defined $ImportantDates{$short};
        $year->{$nm}->{$nd} = [@store];
        $gmnext = gmtime(str2time($gmnext)+60*60*24);
        ($nd,$nm,$ny) = TreeDate($gmnext);
        delete $ImportantDates{$winsol} if defined $ImportantDates{$winsol};
    }
    return $year;
}

sub MakeMonth {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    my %month;
    my %month2;
    my $gmfirst = Moon2GMTime(LastMoon($gmtime));
    my $gmnext = $gmfirst;
    my ($fd,$fm,$fy) = TreeDate($gmfirst);
    my ($nd,$nm,$ny) = ($fd,$fm,$fy);
    while ($nm eq $fm){
      $month{"$nd/$nm/$ny"} = $gmnext;
      $month{"$nd/$nm/$ny"} =~ s/\d\d\:\d\d\:\d\d\s//;
      $month2{$month{"$nd/$nm/$ny"}} = "$nd/$nm/$ny";
      $gmnext = gmtime(str2time($gmnext)+60*60*24);
      ($nd,$nm,$ny) = TreeDate($gmnext);
    }
    return {%month},{%month2};
}

sub TreeDate {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    $gmtime =~ /(\d\d\d\d)/;
    my $year = $1;
    my $time = str2time($gmtime);
    if(TimeSinceEquinox($gmtime) < 60*60*24*30 && $gmtime =~ /dec/i){
        $year ++;
    }
    warn "Couldn't determine year\n" unless $year;
    my $apartmoon = LastMoon("Feb 2 00:00:00 $year")-2;
    my $currentmoon = LastMoon($gmtime) - $apartmoon;
    my $moonmonth = (qw(DAp Bir Row Ash Ald Wil Haw Oak Hol Haz Vin Ivy Bla Eld))[$currentmoon];
    my $monthday = int((GMTime2Moon($gmtime) - LastMoon($gmtime)) * 29.6) + 1;

    $monthday = "0$monthday" if $monthday < 10;

    return ($monthday,$moonmonth,$year);
}

sub LastMoon {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    # this function now gets the number of the moon for time and floors it
    return int(GMTime2Moon($gmtime));
}

sub NextMoon {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    # this function now gets the number of the moon for time and floors it
    return int(GMTime2Moon($gmtime)) + 1;
}

sub LastMoonGMTime {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    # this function now gets the number of the moon for time, floors it and works
    # out the gmtime again
    return Moon2GMTime(int(GMTime2Moon($gmtime)));
}

sub NextMoonGMTime {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    # this function now gets the number of the moon for time, floors it and works
    # out the gmtime again
    return Moon2GMTime(1+int(GMTime2Moon($gmtime)));
}

# Foundation Functions:
# Moon2Time: number (and fraction) of moons since the first moon after 2000-01-01
#             00:00:00,  returns time
# Moon2GMTime: number (and fraction) of moons since the first moon after 2000-01-01
#             00:00:00, returns gmtime
# Time2Moon: time, returns number (and fraction) of moons since the first moon
#             after 2000-01-01 00:00:00
# GMTime2Moon: gmtime, returns number (and fraction) of moons since the first moon
#             after 2000-01-01 00:00:00
# QuadraticFormula: y,a,b,c where y = axx + bx + c, returns one value of x
#             corresponding to x=(-b+(b*b-4*a*c)^0.5)/(2*a)
#             (      the alternative is x=(-b-(b*b-4*a*c)^0.5)/(2*a)          )

sub test {
    print "Moon2Time,2 = ".Moon2Time(2)."\n";
    print "Moon2GMTime,2 = ".Moon2GMTime(2)."\n";
    print "Time2Moon,952271259.815935 = ". Time2Moon(952271259.815935)."\n";
    print "GMTime2Moon,Sun Mar  5 15:47:39 2000 = ". GMTime2Moon("Sun Mar  5 15:47:39 2000")."\n";
}

test();

sub TimeSinceEquinox {
    my ($gmtime) = @_;
    $gmtime = $gmtime || gmtime(time());
    my $time = str2time($gmtime);
    my $days = $time / (24*60*60);
    my $dayssinceequinox = ((($days-$firstepochequinox)/$yearlength) -
                            int(($days-$firstepochequinox)/$yearlength))*$yearlength;
    return $dayssinceequinox*24*60*60;
}

sub Moon2Time {
    my ($moon) = @_;
    $daysSinceY2K = 5.597661 + 29.5305888610 * $moon
            + (102.026e-12) * $moon * $moon
            - 0.000739 - (235.0e-12) * $moon * $moon;
            # the last line here is the correction for UTC
    my $time = $daysSinceY2K * 24 * 60 * 60 + $y2k;
    return $time;
}

sub Moon2GMTime {
    my ($moon) = @_;
    return gmtime(Moon2Time($moon));
}

sub Time2Moon {
    my ($time) = @_;
    my $daysSinceY2K = ($time - $y2k) / (24 * 60 * 60);
    my $a = (102.026e-12) - (235.0e-12);
    my $b = 29.5305888610;
    my $c = 5.597661 - 0.000739;
    my $moon = QuadraticFormula($daysSinceY2K,$a,$b,$c);
    return $moon;
}

sub GMTime2Moon {
    my ($gmtime) = @_;
    my $time = str2time($gmtime);
    return Time2Moon($time);
}

sub QuadraticFormula {
    my ($y,$a,$b,$c) = @_;
    # where y = axx + bx + c
    # or 0 = axx + bx + (c - y)
    return (- $b + sqrt($b*$b - 4*$a*($c-$y)))/(2*$a);
}

