#!/usr/bin/perl
use strict;
use warnings;
use Astro::MoonPhase; # there's good stuff in here!

my ($A,$B,$C) = (0,1,0);
my $time = 1 + ($ARGV[0] + 30) * 365.25 * 24 * 60 * 60;
my @p = phase($time);
my $i = $#p;
my ($this,$last) = ($p[$i],$p[$i]);
my $day = 24*60*60;
my $inc = $day; # find the day
my $angle = 0.5419787;
while(1){
	my @p = phase($time);
	$this = $p[$i];
	if($angle>$last && $angle<$this){
		if($inc > 1){
			$time-=$inc;
			my @p = phase($time);
			($this,$last) = ($p[$i],$p[$i]);

			$inc /= 10;
			next;
		}
		#found the max
		print scalar gmtime ($time-1);
		print "\n$this\n";
		last;
	}
	$time+=$inc;
	$last = $this;
}

